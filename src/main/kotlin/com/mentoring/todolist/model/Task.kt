package com.mentoring.todolist.model

data class Task(val id: Int, val title: String, val description: String, val status: String)

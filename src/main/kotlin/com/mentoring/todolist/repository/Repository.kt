package com.mentoring.todolist.repository

import com.mentoring.todolist.model.Task

class Repository {
    val tasks = mutableListOf<Task>()

    fun list(): List<Task> {
        return tasks
    }

    fun insert(task: Task) {
        tasks.add(task)
    }
}
package com.mentoring.todolist.configuration

import com.mentoring.todolist.model.Task
import com.mentoring.todolist.repository.Repository
import com.mentoring.todolist.service.TaskService
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post

fun Routing.todo() {
    val repository = Repository()
    val service = TaskService(repository)

    get("/todos") {
        call.respond(service.list())
    }
    post ("/todos") {
        val task = call.receive<Task>()
        service.insert(task)
        call.respond(HttpStatusCode.Created)

    }
}

//curl -l "http://localhost:8080/todos"

//Task(1, "Dever de casa", "Dever que eu tenho que fazer",  Date(), Date(), "finalizado")
package com.mentoring.todolist.configuration

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.ContentType
import io.ktor.jackson.JacksonConverter
import io.ktor.jackson.jackson
import io.ktor.routing.routing
import org.slf4j.event.Level

fun Application.module() {
    install(CallLogging) {
        level = Level.INFO
    }

    install(DefaultHeaders)

    //converte o conteúdo da negociação
    install(ContentNegotiation) {
        jackson {
            register(ContentType.Application.Json, JacksonConverter())
        }
    }

    routing {
        todo()
    }
}
package com.mentoring.todolist.service

import com.mentoring.todolist.model.Task
import com.mentoring.todolist.repository.Repository

class TaskService(private val repository: Repository) {
    fun list(): List<Task> {
        return repository.list()
    }

    fun insert(task: Task) {
        repository.insert(task)
    }

}